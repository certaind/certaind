Tips for setting up certaind for development
============================================

As the superuser, install the expected dependencies:

    # apt install postgresql postgresql-client python3-psycopg2 xdg-user-dirs

If https://bugs.debian.org/891687 is merged, then just:

    $ systemctl --user start pgcluster@certaind

Until then:

    $ mkdir ~/.config/certaind
    $ /usr/lib/postgresql/10/bin/initdb -D ~/.config/certaind/postgres --auth-local=peer
    $ echo "listen_addresses = ''" >> ~/.config/certaind/postgres/postgresql.conf
    $ cp certaind.service ~/.config/systemd/user/
    $ systemctl --user daemon-reload
    $ systemctl --user start certaind
    $ PGHOST=$XDG_RUNTIME_DIR/certaind createdb certaind

Now you can set up the database:

    $ ./devel/resetdb

You can now explore the db manually:

    $ PGHOST=$XDG_RUNTIME_DIR/certaind psql certaind
    psql (10.2 (Debian 10.2-1))
    Type "help" for help.
    
    certaind=# 
    ...

Or use the certaind command line interface:

    $ python3 -m certaind import - < /usr/share/keyrings/debian-keyring.gpg
    852 certificates processed.
    $ python3 -m certaind exec 'select full_text from uid' | wc
       3072   10444  137419
    $ 
