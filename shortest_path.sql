-- Calculate shortest binding path between two user IDs
--
-- Path is mapped from issuer to subject key along the uid_binding
-- chain, with the associated full text of the UID for the binding
-- appended to the path chain to be returned.  Algorithm terminates
-- when reaching a binding for the target UID.
--
-- Returns two equal-length arrays for the shortest path: the issuer
-- keys and UID full text for each binding in the chain.
--
-- Lifted almost verbatim from examples in the "WITH Queries" section
-- of the PostgreSQL documentation:
--
-- https://www.postgresql.org/docs/10/static/queries-with.html

CREATE OR REPLACE FUNCTION shortest_path(origin text, target text)
RETURNS TABLE(shortest_key_path bytea[], shortest_uid_path text[]) AS
$BODY$
BEGIN

    RETURN QUERY WITH RECURSIVE paths(before_tail, tail, key_path, uid_path) AS (
         SELECT
             NULL::bytea,
             uid_binding.issuer_key,
             array[uid_binding.issuer_key],
             array[origin]
         FROM
             uid_binding
         JOIN
             uid ON (uid.id = uid_binding.uid)
         WHERE
             origin = uid.full_text
             AND
             uid_binding.subject_key = uid_binding.issuer_key

      UNION

         SELECT
             tail,
             subject_key,
             key_path || subject_key,
             uid_path || uid.full_text
         FROM
             paths
         JOIN
             uid_binding ON (tail = issuer_key
                             AND
                             NOT subject_key = ANY(key_path))
         JOIN
             uid ON (uid.id = uid_binding.uid)
    )
    SELECT
        key_path, uid_path
    FROM
        paths
    WHERE
        uid_path[array_length(uid_path, 1)] = target
    LIMIT
        1
    ;
END;
$BODY$
  LANGUAGE plpgsql;
