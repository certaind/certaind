-- This defines the certaind postgresql database

create table packettype (
 id smallint primary key,
 label text not null
 );
insert into packettype (id, label) values (0, 'Reserved');
insert into packettype (id, label) values (1, 'Public-Key Encrypted Session Key Packet');
insert into packettype (id, label) values (2, 'Signature Packet');
insert into packettype (id, label) values (3, 'Symmetric-Key Encrypted Session Key Packet');
insert into packettype (id, label) values (4, 'One-Pass Signature Packet');
insert into packettype (id, label) values (5, 'Secret Key Packet');
insert into packettype (id, label) values (6, 'Public Key Packet');
insert into packettype (id, label) values (7, 'Secret Subkey Packet');
insert into packettype (id, label) values (8, 'Compressed Data Packet');
insert into packettype (id, label) values (9, 'Symmetrically Encrypted Data Packet');
insert into packettype (id, label) values (10, 'Marker Packet');
insert into packettype (id, label) values (11, 'Literal Data Packet');
insert into packettype (id, label) values (12, 'Trust Packet');
insert into packettype (id, label) values (13, 'User ID Packet');
insert into packettype (id, label) values (14, 'Public Subkey Packet');
insert into packettype (id, label) values (17, 'User Attribute Packet');
insert into packettype (id, label) values (18, 'Symmetrically Encrypted and MDC Packet');
insert into packettype (id, label) values (19, 'Modification Detection Code Packet');
       
create table pubkeytype (
 id smallint primary key,
 label text not null
 );
insert into pubkeytype (id, label) values (1, 'RSA Encrypt or Sign');
insert into pubkeytype (id, label) values (2, 'RSA Encrypt-Only');
insert into pubkeytype (id, label) values (3, 'RSA Sign-Only');
insert into pubkeytype (id, label) values (16, 'ElGamal Encrypt-Only');
insert into pubkeytype (id, label) values (17, 'DSA Digital Signature Algorithm');
insert into pubkeytype (id, label) values (18, 'Elliptic Curve');
insert into pubkeytype (id, label) values (19, 'ECDSA');
insert into pubkeytype (id, label) values (20, 'Formerly ElGamal Encrypt or Sign');
insert into pubkeytype (id, label) values (21, 'Diffie-Hellman');
insert into pubkeytype (id, label) values (22, 'EdDSA');

create table sigtype (
 id smallint primary key,
 label text not null
 );
insert into sigtype (id, label) values (0, 'Signature of a binary document');
insert into sigtype (id, label) values (1, 'Signature of a canonical text document');
insert into sigtype (id, label) values (2, 'Standalone signature');
insert into sigtype (id, label) values (16, 'Generic certification of a User ID and Public Key packet');
insert into sigtype (id, label) values (17, 'Persona certification of a User ID and Public Key packet');
insert into sigtype (id, label) values (18, 'Casual certification of a User ID and Public Key packet');
insert into sigtype (id, label) values (19, 'Positive certification of a User ID and Public Key packet');
insert into sigtype (id, label) values (24, 'Subkey Binding Signature');
insert into sigtype (id, label) values (25, 'Primary Key Binding Signature');
insert into sigtype (id, label) values (31, 'Signature directly on a key');
insert into sigtype (id, label) values (32, 'Key revocation signature');
insert into sigtype (id, label) values (40, 'Subkey revocation signature');
insert into sigtype (id, label) values (48, 'Certification revocation signature');
insert into sigtype (id, label) values (64, 'Timestamp signature');
insert into sigtype (id, label) values (80, 'Third-Party Confirmation signature');

create table hashalgo (
 id smallint primary key,
 label text not null
 );
insert into hashalgo (id, label) values (1, 'MD5');
insert into hashalgo (id, label) values (2, 'SHA-1');
insert into hashalgo (id, label) values (3, 'RIPE-MD/160');
insert into hashalgo (id, label) values (8, 'SHA256');
insert into hashalgo (id, label) values (9, 'SHA384');
insert into hashalgo (id, label) values (10, 'SHA512');
insert into hashalgo (id, label) values (11, 'SHA224');


create type usageflag as enum (
 'cert',
 'sign',
 'enccoms',
 'encstore',
 'split',
 'auth',
 'group'
 );

create table packet (
 data bytea not null,
 tag smallint references packettype(id) not null
 );

create table pubkey (
 algo smallint references pubkeytype(id) not null,
 creation timestamp not null,
 expiration timestamp,
 keyid bigint not null,
 fpr bytea check (length(fpr) = 20) primary key,
 version smallint not null,
 unique (tag, fpr)
 ) inherits (packet);

create index on pubkey(keyid);

create table uid (
 id serial primary key,
 full_text text not null unique,
 display_name text,
 email text
 ) inherits (packet);

create table signature (
 version smallint not null,
 sig_type smallint references sigtype(id) not null,
 hashalgo smallint references hashalgo(id) not null,
 subject_key bytea check (length(subject_key) = 20) references pubkey(fpr) not null,
 issuer_key bytea check (length(issuer_key) = 20) references pubkey(fpr),
 issuer_keyid bigint,
 issuer_fpr bytea check (length(issuer_fpr) = 20),
 creation timestamp,
 expiration timestamp,
 usage_flags bytea,
 verified_on timestamp
 ) inherits (packet);

create index on signature(issuer_keyid);

create table uid_binding (
 id serial primary key,
 uid int references uid(id) not null
 ) inherits (signature);

-- constraints do not inherit with tables, so se need all of these:
-- see https://www.postgresql.org/docs/current/static/ddl-inherit.html
create unique index unique_uid_binding_data on uid_binding (data);
create unique index unique_signature_data on signature (data);
create unique index unique_packet_data on packet (data);
create unique index unique_pubkey_data on pubkey (data);
