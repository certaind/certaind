import codecs


# dictionary of bits to usage flags
SIG_USAGE_MAP = {
    0: {
        0x01: 'cert',
        0x02: 'sign',
        0x04: 'enccoms',
        0x08: 'encstore',
        0x10: 'split',
        0x20: 'auth',
        0x80: 'group',
    }
}

def parse_usage(ubytes):
    """Parse OpenPGP usage byte(s) into set of usage flags."""
    ret = set()
    for byte,fdict in SIG_USAGE_MAP.items():
        b = ubytes[ind]
        for bit,flag in fdict.items():
            if b & bit:
                ret.add(flag)
    return ret


def keyid_to_bigint(keyid):
    """An OpenPGP Key ID is 64 bits.  So is a postgresql bigint."""
    if keyid is None:
        return None
    return int.from_bytes(codecs.decode(keyid, 'hex'), byteorder='big', signed=True)


def bigint_to_keyid(bigint):
    """A postgresql bigint is 64 bits.  So is an OpenPGP Key ID."""
    if bigint is None:
        return None
    return codecs.encode(bigint.to_bytes(8, byteorder='big', signed=True), 'hex')


def create_packet(tag, data):
    """Create an OpenPGP packet from a tag and data block."""
    dlen = len(data)
    if tag < 16:
        # old packet format
        oct0 = 0x80 | (tag<<2)
        length = tag & 0x03
        if dlen < 1<<8:
            header = bytearray((oct0, dlen))
        elif dlen < 1<<16:
            header = bytearray((oct0 | 1, dlen//256, dlen&0xff))
        elif dlen < 1<<32:
            header = bytearray((oct0 | 2, (dlen>>24)&0xff, (dlen>>16)&0xff, (dlen>>8)&0xff, (dlen)&0xff))
        else:
            header = bytearray((oct0 | 3,))
    else:
        # FIXME: generate new packet format header
        raise NotImplementedError("New packet format needed!")
    return header + bytearray(data)
