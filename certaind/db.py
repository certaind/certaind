import os
import pgpy
import psycopg2
import codecs
import logging

from . import insert
from . import util


try:
    runtime_dir = os.environ['XDG_RUNTIME_DIR']
except KeyError:
    import posix
    runtime_dir = os.path.join('run', 'user', str(posix.getuid()))
# FIXME: is this the right place to set this variable?
os.environ['PGHOST'] = os.path.join(runtime_dir, 'certaind')


def decode_fpr(fpr_bytes):
    """decode database fingerprint bytes into ascii string"""
    return codecs.encode(fpr_bytes, 'hex').decode('ascii').upper()


def encode_fpr(fpr_string):
    """encode ascii string into database fingerprint bytes"""
    return codecs.decode(fpr_string, 'hex')


class Certaind:
    # FIXME: create db if it doesn't exist:
    #   $ createdb certaind
    # FIXME: create tables if they don't exist:
    #   $ psql certaind < certaind.sql

    def __init__(self):
        self.db = None

    def open(self):
        self.db = psycopg2.connect("dbname=certaind")

    def close(self):
        self.db.close()

    def __enter__(self):
        self.open()
        return self

    def __exit__(self, *exc):
        self.close()

    ##########

    def execute(self, cmd):
        """Execute command on the database

        Yields response records.

        """
        with self.db.cursor() as cur:
            cur.execute(cmd)
            # FIXME: this doesn't seem like the best way to test if
            # there are records to output, but I can't figure out a
            # better way.
            try:
                for record in cur:
                    yield record
            except psycopg2.ProgrammingError:
                pass
        self.db.commit()

    ##########

    def import_stream(self, bytestream, dry_run=False):
        """Parse packets from byte stream and import into database

        """
        with self.db.cursor() as cur:
            insert.insert_packets(cur, bytestream, dry_run=dry_run)
        self.db.commit()


    def verify_sigs(self):
        """Verify all un-verified UID bindings in the database

        This loops through all UID bindings in the database,
        cryptographically verifies the binding, then updates the issuer
        key in the binding if successfully verified.

        """
        search_cmd = """
select
  uid_binding.data, uid_binding.tag, uid_binding.id,
  issuer.data, issuer.tag, issuer.fpr,
  subject.data, subject.tag,
  uid.full_text
from
  uid_binding
join
  pubkey as issuer on (issuer.keyid = uid_binding.issuer_keyid)
join
  pubkey as subject on (subject.fpr = uid_binding.subject_key)
join
  uid on (uid.id = uid_binding.uid)
where
  uid_binding.issuer_key is null
"""

        update_cmd = """
update
  uid_binding
set
  issuer_key = %s
where
  uid_binding.id = %s
"""

        with self.db.cursor() as cursor:
            cursor.execute(search_cmd)
            for row in cursor:
                u_data, u_tag, u_id, i_data, i_tag, i_id, s_data, s_tag, uid_text = row

                signature = pgpy.PGPSignature.from_blob(util.create_packet(u_tag, u_data))
                uid = pgpy.PGPUID.new(uid_text)
                issuer = pgpy.PGPKey.from_blob(util.create_packet(i_tag, i_data))[0]
                subject = pgpy.PGPKey.from_blob(util.create_packet(s_tag, s_data))[0]
                subject.add_uid(uid | signature, selfsign=False)

                try:
                    verified = issuer.verify(subject)
                except AttributeError:
                    # AttributeError: module 'cryptography.hazmat.primitives.hashes' has no attribute 'RIPEMD160'
                    verified = False

                if not verified:
                    logging.info("binding not verified: issuer={}, subject={}, uid={}".format(
                        issuer.fingerprint.replace(' ', ''),
                        subject.fingerprint.replace(' ', ''),
                        uid.name))
                    continue

                with self.db.cursor() as cursor2:
                    cursor2.execute(update_cmd, (i_id, u_id))

        self.db.commit()

    ##########

    def keys_for_uid(self, uid):
        """Generator of all fingerprints for pubkeys to which a given UID is bound

        """
        cmd = """
select distinct
  pubkey.fpr
from
  pubkey
join
  uid_binding on (pubkey.fpr = uid_binding.subject_key)
join
  uid on (uid_binding.uid = uid.id)
where
  uid_binding.issuer_key is not null
  and uid.full_text = %s
"""
        with self.db.cursor() as cur:
            cur.execute(cmd, (uid,))
            for out, in cur:
                yield decode_fpr(out)


    def uids_for_key(self, pubkey):
        """Generator of all UIDs bound to a given pubkey fingerprint

        """
        cmd = """
select distinct
  uid.full_text
from
  uid
join
  uid_binding on (uid.id = uid_binding.uid)
join
  pubkey on (uid_binding.subject_key = pubkey.fpr)
where
  uid_binding.issuer_key is not null
  AND pubkey.fpr = %s
"""
        with self.db.cursor() as cur:
            cur.execute(cmd, (encode_fpr(pubkey),))
            for out, in cur:
                yield out


    def shortest_path(self, uid0, uid1):
        """Find shortest binding path between two UIDs

        UIDs should be full UID text string.  Path follows from issuer
        to subject.

        Returns list of (key, uid) tuples.

        """
        cmd = "select * from shortest_path(%s, %s)"
        with self.db.cursor() as cur:
            cur.execute(cmd, (uid0, uid1))
            key_path, uid_path = cur.fetchone()
        return zip(map(decode_fpr, key_path),
                   uid_path)
