import io
import codecs
import pgpdump
import logging

from . import util

#####################################################

def find_id(cursor, table, **kwargs):
    """find index of row matching keyword args

    keyword args joined with AND.

    Should find either 1 or 0 matching rows.  If more are found, an
    exception will be raised.

    """
    fields = ' and '.join(['{}=%s'.format(k) for k in kwargs])
    cmd = "select id from {table} where {fields}".format(
        table=table, fields=fields)
    cursor.execute(cmd, tuple(kwargs.values()))
    result = cursor.fetchone()
    if result:
        if cursor.fetchone():
            raise Exception('Found more than one answer searching %s for %s'%(table, kwargs))
        return result[0]
    else:
        return None


def insert_row(cursor, table, on_conflict=None, dry_run=False, **kwargs):
    """convenience function to insert rows into table

    kwargs are column field/value keywords.

    returns True if insert happened, False otherwise

    """
    if dry_run:
        return False
    cols = ','.join(kwargs.keys())
    fmt = ','.join(['%s']*len(kwargs))
    cmd = 'insert into {table} ({cols}) values ({fmt})'.format(
        table=table, cols=cols, fmt=fmt)
    if on_conflict:
        cmd += ' on conflict {action}'.format(
            action=on_conflict)
    cursor.execute(cmd, tuple(kwargs.values()))
    # See "Outputs":
    # https://www.postgresql.org/docs/10/static/sql-insert.html
    return int(cursor.statusmessage.split(' ')[2]) > 0

#####################################################

def insert_packets(cursor, bytestream, dry_run=False):
    """parse and insert packets from packet iterator

    First argument should be a db cursor

    """
    idata = io.BytesIO()
    for line in bytestream:
        idata.write(line)
    idata.seek(0)

    # FIXME: auto-detect binary/ascii input
    packet_iter = pgpdump.BinaryData(idata.read()).packets

    N_certs = 0
    N_certs_new = 0
    N_certs_updated = 0
    N_cert_packets_imported = 0
    N_packets = 0
    N_packets_imported = 0
    success = False
    known_cert = False

    for packet in packet_iter():
        N_packets += 1

        # PUBLIC KEY PACKETS
        if isinstance(packet, pgpdump.packet.PublicKeyPacket):
            logging.debug(packet.fingerprint.decode())

            key_fpr = codecs.decode(packet.fingerprint, 'hex')

            success = insert_row(
                cursor, 'pubkey',
                on_conflict='do nothing',
                dry_run=dry_run,

                data=packet.data,
                tag=packet.raw,
                algo=packet.raw_pub_algorithm,
                creation=packet.creation_time,
                expiration=packet.expiration_time,
                keyid=util.keyid_to_bigint(packet.key_id),
                fpr=key_fpr,
                version=packet.pubkey_version,
            )

            # PUB KEY PACKET
            if type(packet) is pgpdump.packet.PublicKeyPacket:
                # tag == 6
                N_certs += 1
                if success:
                    N_certs_new += 1
                    known_cert = False
                else:
                    known_cert = True
                N_cert_packets_imported = 1
                mode = 'direct'
                pubkey_fpr = key_fpr

            # SUB KEY PACKET
            elif type(packet) is pgpdump.packet.PublicSubkeyPacket:
                # tag == 14
                mode = 'subkey'
                subkey_fpr = key_fpr

        # UID PACKET
        elif type(packet) is pgpdump.packet.UserIDPacket:
            logging.debug(packet.user)

            success = insert_row(
                cursor, 'uid',
                on_conflict='do nothing',
                dry_run=dry_run,

                data=packet.data,
                tag=packet.raw,
                full_text=packet.user,
                display_name=packet.user_name,
                email=packet.user_email,
            )

            uid_id = find_id(cursor, 'uid', full_text=packet.user)

            mode = 'uid'

        # SIGNATURE/BINDING PACKET
        elif type(packet) is pgpdump.packet.SignaturePacket:

            # parse subkey packets for usage flags

            flags = None
            for sub in packet.subpackets:
                if sub.hashed and sub.name == 'Key Flags':
                    if flags is not None:
                        raise RuntimeError("Multiple key flags for subkey!")
                    # FIXME: should better parse usage flags.  instead
                    # for now just shove whole usage byte into db
                    # flags = util.parse_usage(sub.data)
                    flags = sub.data
                # FIXME: handle "cross sigs" of subkey over primary key
                if sub.name == 'Embedded Signature':
                    pass

            if mode == 'uid':
                success = insert_row(
                    cursor, 'uid_binding',
                    on_conflict='do nothing',
                    dry_run=dry_run,

                    data=packet.data,
                    tag=packet.raw,
                    version=packet.sig_version,
                    sig_type=packet.raw_sig_type,
                    hashalgo=packet.raw_hash_algorithm,
                    uid=uid_id,
                    subject_key=pubkey_fpr,
                    issuer_key=None,
                    issuer_keyid=util.keyid_to_bigint(packet.key_id),
                    creation=packet.creation_time,
                    expiration=packet.expiration_time,
                    usage_flags=flags,
                )

            elif mode == 'subkey':
                success = insert_row(
                    cursor, 'signature',
                    on_conflict='do nothing',
                    dry_run=dry_run,

                    data=packet.data,
                    tag=packet.raw,
                    version=packet.sig_version,
                    sig_type=packet.raw_sig_type,
                    hashalgo=packet.raw_hash_algorithm,
                    subject_key=subkey_fpr,
                    issuer_key=pubkey_fpr,
                    issuer_keyid=util.keyid_to_bigint(packet.key_id),
                    creation=packet.creation_time,
                    expiration=packet.expiration_time,
                    usage_flags=flags,
                )

        if success:
            N_packets_imported += 1
            N_cert_packets_imported += 1
        if known_cert and N_cert_packets_imported > 1:
            N_certs_updated += 1

    logging.info("{} certificates processed, {} new, {} updated ({}/{} packets imported)".format(
        N_certs, N_certs_new, N_certs_updated, N_packets_imported, N_packets))
