import os
import signal
import fileinput
import argparse
import logging

from .db import Certaind

##########

parser = argparse.ArgumentParser()

subparser = parser.add_subparsers(
    title='Commands',
    metavar='<command>',
    dest='cmd',
    #help=argparse.SUPPRESS,
)
subparser.required = True

def gen_subparser(cmd, func, node_nargs=None):
    help = func.__doc__.split('\n')[0].lower().strip('.')
    desc = func.__doc__
    p = subparser.add_parser(
        cmd,
        #formatter_class=argparse.RawDescriptionHelpFormatter,
        help=help,
        description=desc,
    )
    p.set_defaults(func=func)
    if node_nargs:
        p.add_argument('nodes', metavar='node', nargs=node_nargs,
                       help="guardian node name (may be wildcard/glob pattern)")
    return p

##################################################

def command_help(args):
    """show command help"""
    if args.cmd:
        try:
            p = subparser.choices[args.cmd]
        except KeyError:
            sys.exit("Unknown command: {}".format(args.cmd))
    else:
        p = parser
    p.print_help()

p = gen_subparser("help", command_help)
p.add_argument('cmd', metavar='<command>', nargs='?', help="command")

##########

def import_keys(args):
    """import keys from files or stdin

    """
    # file:///usr/share/doc/python3.6-doc/html/library/fileinput.html
    with fileinput.input(files=args.file, mode='rb') as f:
        with Certaind() as db:
            db.import_stream(f, dry_run=args.dry_run)

p = gen_subparser("import", import_keys)
p.add_argument('file', nargs='*', help="file path or '-' for stdin")
p.add_argument('-d', '--dry-run', action='store_true',
               help="process keys without importing")

##########

def verify_sigs(args):
    """verify all un-verified user ID signatures in database

    """
    with Certaind() as db:
        db.verify_sigs()

p = gen_subparser("verify", verify_sigs)

##########

def keys_for_uid(args):
    """print all public keys a given user ID is bound to

    """
    with Certaind() as db:
        for fpr in db.keys_for_uid(args.uid):
            print(fpr)

p = gen_subparser("keys-for-uid", keys_for_uid)
p.add_argument('uid', help="user ID")

##########

def uids_for_key(args):
    """print all user IDs bound to a given public key

    """
    with Certaind() as db:
        for fpr in db.uids_for_key(args.pubkey):
            print(fpr)

p = gen_subparser("uids-for-key", uids_for_key)
p.add_argument('pubkey', help="public key fingerprint")

##########

def shortest_path(args):
    """print all user IDs in binding path between two user IDs

    """
    with Certaind() as db:
        for key, uid in db.shortest_path(args.uid0, args.uid1):
            print('{} {}'.format(key, uid))

p = gen_subparser("path", shortest_path)
p.add_argument('uid0', help="start UID")
p.add_argument('uid1', help="end UID")

##########

def exec_command(args):
    """exec postgres command and print result lines

    """
    cmd = ' '.join(args.cmd)
    with Certaind() as db:
        for out in db.execute(cmd):
            print(out)

p = gen_subparser("exec", exec_command)
p.add_argument('cmd', nargs='*', help="command")

##################################################

def main():
    signal.signal(signal.SIGINT, signal.SIG_DFL)

    logging.basicConfig(format='%(message)s',
                        level=os.getenv('LOG_LEVEL', 'INFO').upper())

    args = parser.parse_args()
    args.func(args)

if __name__ == '__main__':
    main()
